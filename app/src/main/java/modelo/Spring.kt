package modelo

class Spring {
    private var id: Long = 0
    private lateinit var movie_name: String
    private lateinit var url: String

    fun getId(): Long{
        return this.id
    }

    fun getMovie_Name() : String{
        return this.movie_name
    }

    fun getUrl() : String{
        return this.url
    }

    fun setId(id : Long) {
        this.id = id
    }

    fun setMovie_Name(movie_name : String) {
        this.movie_name = movie_name
    }

    fun setUrl(url : String) {
        this.url
    }
}