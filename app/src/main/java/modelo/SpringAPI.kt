package modelo

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface SpringAPI {
    @Headers("Accept: application/json")
    // Método para obtener todas las películas
    @GET("movies")
    fun getSpring(): Call<List<Spring>>
    // Método para obtener una película por su ID
    @GET("spring/{id}")
    fun getPokemon(@Path("id") id: Long): Call<Spring>
}