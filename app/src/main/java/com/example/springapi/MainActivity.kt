package com.example.springapi

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.springapi.ui.theme.SpringApiTheme
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import modelo.SpringAPI
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : ComponentActivity() {

    //Es global, tiene que estar fuera del onCreate
    private lateinit var retrofit: Retrofit
    private var texto: String = "Spring"
    private var nombres: MutableState<List<String>> = mutableStateOf(emptyList())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        retrofit = retrofit2.Retrofit.Builder()
            .baseUrl("http://10.0.2.2:8080/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        texto = obtenerDatos(retrofit)
        obtenerDatos(retrofit)

        setContent {
            SpringApiTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MySpringApi(nombres = nombres.value)
                }
            }
        }
    }

    private fun obtenerDatos(retrofit: Retrofit): String {
        var texto = "";

        CoroutineScope(Dispatchers.IO).launch {

            val call = retrofit.create(SpringAPI::class.java).getSpring().execute()
            val movies = call.body()
            if (call.isSuccessful) {
                val nombresList = movies?.map { it.getMovie_Name() } ?: emptyList()
                nombres.value = nombresList

                texto = movies?.get(0)?.getMovie_Name().toString()
            } else {
                texto = "Ha habido un error";
            }
        }
        Thread.sleep(1000)
        return texto;
    }

}

@Composable
fun VerticalSpacer(height: Dp) {
    Spacer(modifier = Modifier.height(height))
}

@Composable
fun MySpringApi(nombres: List<String>) {

    Column {
        nombres.forEach { nombre ->
            Text(text = nombre)
            VerticalSpacer(height = 16.dp)
        }
    }
}